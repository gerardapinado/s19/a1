console.log(`S19 ES6 Activity`)

//3
let num = 2
let getCube = num ** 3
//4
console.log(`The cube of ${num} is ${getCube}`)

//5
let address = ["258", "Washington Ave NW", "Califonia", "90011"]
//6
let [st, city, state, zip] = address
console.log(`I live at ${st} ${city}, ${state} ${zip}`)

//7
let animal = {
	name: "lolong",
	family: "crocodile",
	type: "saltwater",
	weight: 1075,
	measurement: [20, 3]
}
//8
let {name,family,type,weight,measurement} = animal
console.log(`${name} was a ${type} ${family}. He weighed at ${weight} kgs with a measurement of ${measurement[0]} ft ${measurement[1]} in.`)

//9
let arrOfNums = [1,2,3,4,5]
//10
arrOfNums.forEach(element => console.log(element))


//11
let reduceNumber = 0 
reduceNumber = arrOfNums.reduce((num1,num2) => num1 + num2)
console.log(reduceNumber)

//12
class Dog{
	constructor(name,age,breed){
		this.name = name
		this.age = age  
		this.breed = breed
	}
}
//13
let poodle = new Dog("Frankie", 5, "Miniature Dachshund")
console.log(poodle)